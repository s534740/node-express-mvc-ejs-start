/** 
*  user controller
*  Handles requests related to users (see routes)
*
* @author Denise Case <dcase@nwmissouri.edu>
*
*/
const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/user.js')
const notfoundstring = 'user not found'

// RESPOND WITH JSON DATA  --------------------------------------------

// GET all JSON
api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.users.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.num)
  const data = req.app.locals.users.query
  const item = find(data, { _id : id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

// RESPOND WITH VIEWS  --------------------------------------------

// GET to this controller base URI (the default)
api.get('/', (req, res) => {
  res.render('user/index.ejs')
})

// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('user/create',
    {
      title: 'Create user',
      layout: 'layout.ejs',
      user: item
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.num)
  const data = req.app.locals.users.query
  const item = find(data, { _id : id  })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('user/delete.ejs',
    {
      title: 'Delete user',
      layout: 'layout.ejs',
      user: item
    })
})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.users.query
  const item = find(data, { _id : id  })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('user/details.ejs',
    {
      title: 'user Details',
      layout: 'layout.ejs',
      user: item
    })
})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.num)
  const data = req.app.locals.users.query
  const item = find(data, { _id : id  })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('user/edit.ejs',
    {
      title: 'users',
      layout: 'layout.ejs',
      user: item
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.users.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body._id}`)
  item.user_name = req.body.user_name
  item.account_number = req.body.account_number
  item.user_email= req.body.user_email
  item.balance = parseInt(req.body.balance)
  item.phone_number = parseInt(req.body.phone_number)
  
  data.push(item)
  LOG.info(`SAVING NEW user ${JSON.stringify(item)}`)
  return res.redirect('/user')
})

// POST update
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const num = parseInt(req.params.num)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.users.query
  const item = find(data, { _id : id  })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.user_name = req.body.user_name
  item.account_number = req.body.account_number
  item.user_email= req.body.user_email
  item.balance = parseInt(req.body.balance)
  item.phone_number = parseInt(req.body.phone_number)
  LOG.info(`SAVING UPDATED user ${JSON.stringify(item)}`)
  return res.redirect('/user')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const num = parseInt(req.params.num)
  LOG.info(`Handling REMOVING ID=${num}`)
  const data = req.app.locals.users.query
  const item = find(data, { _id : id  })
  if (!item) { return res.end(notfoundstring) }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { account_number: num })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
  return res.redirect('/user')
})

module.exports = api
