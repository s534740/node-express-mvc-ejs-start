const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  Account_Number: { type: Number, required: true },
  Account_Name: { type: "String", required: true },
  Account_balance: { type: Number, required: true},
  Account_SSN: { type: Number, required: true}
})

module.exports = mongoose.model('Account', AccountSchema)