/** 
*  Transaction Item model
*  Describes the characteristics of each attribute in an Transaction.
*
* @author Sravya Katpally<S534740@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionNum: { type: Number, required: true, unique: true, default: 555 },
  transactionType: { type: String, required: true },
  balance: {type: Number,required: true},
  dateOfTransaction: { type: String, required: true, default: Date.now() },
  
})

module.exports = mongoose.model('Transaction', TransactionSchema)
