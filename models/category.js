/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*
* @author Rajesh Tarigopula <S534888@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  Account_Number: { type: "String", required: true },
  Account_type: { type:"String", required: true, default: '  ' },
  Credit_card_number: { type: Number, required: false},
  Debit_card_number: { type: Number, required: true}
})

module.exports = mongoose.model('Category', CategorySchema)